//
//  HelperFiles.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 08/01/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import Foundation
import UIKit

//Global variables
struct GlobalVariables {
    static let blue = UIColor.init(red: 52, green: 178, blue: 224, alpha: 1)
    static let purple = UIColor.init(red: 161, green: 114, blue: 255, alpha: 1)
}


//Extensions
class RoundedImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.width / 2.0
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}

extension UIColor {
    class func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}


class RoundedButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.height / 2.0
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}

//Enums
enum Gender {
    case male
    case female
}

enum MessageOwner {
    case sender
    case receiver
}

enum ShowExtraView {
    case contacts
    case profile
    case preview
    case map
}

enum MessageType : String {
    case text = "text"
}
