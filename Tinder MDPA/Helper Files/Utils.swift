//
//  Utils.swift
//  Tinder MDPA
//
//  Created by Lluís Criado on 18/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import Foundation
import UIKit

public class Utils: NSObject {

    static func jsonArrayToList(type: String, jsonArray: [[String: Any]]) -> [Any]? {
        switch type {
        case "Message":
            var messageArray = [Message]()
            for messageJson in jsonArray {
                messageArray.append(Message(with: messageJson))
            }
            return messageArray

        case "UserSeen":
            var usersSeenArray = [UserSeen]()
            for userSeenJson in jsonArray {
                usersSeenArray.append(UserSeen(with: userSeenJson))
            }
            return usersSeenArray

        default:
            return nil
        }
    }

}
