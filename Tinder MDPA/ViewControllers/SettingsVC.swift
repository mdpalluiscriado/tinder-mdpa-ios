//
//  SettingsVC.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 14/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    // MARK: Properties
    // Settings view
    @IBOutlet var settingsView: SettingsView!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    var settingsViewTopConstraint: NSLayoutConstraint!
    
    // Edit profile view
    @IBOutlet var editProfileVeiw: EditProfileView!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var aboutMeTextField: UITextField!
    @IBOutlet weak var currentJobTextField: UITextField!
    @IBOutlet weak var studiesTextField: UITextField!
    @IBOutlet weak var userImage: UIImageView!

    // Settings main view
    @IBOutlet weak var cloudsView: UIImageView!

    //MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        customization()
        addGestureRecognizer(userImageView)
        
        if let user = TinderManager.sharedInstance.getLoggedUser() {
            setloggedUserInformation(user)
        }
    }

    // MARK: IBActions
    @IBAction func onSaveButtonClick(_ sender: Any) {
        save()
    }

    @IBAction func onLogoutButtonPressed(_ sender: Any) {
        logout()
    }

    // MARK: Private methods
    // MARK: UI methods
    private func customization() {
        settingsView.customizeIn(settingsVC: self)
        editProfileVeiw.customizeIn(settingsVC: self)
    }

    private func switchViews() {
        if(settingsView.topConstraint.constant == 40) {
            settingsView.topConstraint.constant = 1000
            editProfileVeiw.topConstraint.constant = 40
        } else {
            settingsView.topConstraint.constant = 40
            editProfileVeiw.topConstraint.constant = 1000
        }

        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: { self.view.layoutIfNeeded() }) { (finished) in }
    }

    private func addGestureRecognizer(_ imageView: UIImageView) {
        imageView.isUserInteractionEnabled = true

        let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                          action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        switchViews()
    }

    // MARK: Logic methods
    private func save() {
        if let user = TinderManager.sharedInstance.getLoggedUser() {
            user.fromAge = Int(fromTextField.text!)
            user.toAge = Int(toTextField.text!)
            user.descript = aboutMeTextField.text!
            user.currentJob = currentJobTextField.text!
            user.university = studiesTextField.text!

            let successClosure: OnSuccessClosure = { (data) in
                print("[TINDER_DEBUG]: updated logged user")
            }
            
            NetworkManager.sharedInstance.updateUserValues(requestBody: user.toDictionary(),
                                                           onSuccessClousure: successClosure)
            self.switchViews()
        }
    }

    private func logout() {
        // TODO: Remove logged user from memory/persistence/shared preferences
        self.performSegue(withIdentifier: "logout", sender: nil)
    }

    private func setloggedUserInformation(_ user: User) {
        usernameLabel.text = user.username
        userImageView.downloadedFrom(url: URL(string: user.profilePicLink!)!)

        userImage.downloadedFrom(url: URL(string: user.profilePicLink!)!)
        fromTextField.text = String(describing: user.fromAge)
        toTextField.text = String(describing: user.toAge)
        aboutMeTextField.text = user.descript
        currentJobTextField.text = user.currentJob
        studiesTextField.text = user.university
    }
}
