//
//  ConversationsVC.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 14/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class ConversationsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Properties
    // Matches View
    @IBOutlet var matchesView: MatchesView!
    
    // Conversations view
    @IBOutlet weak var tableView: UITableView!
    var conversations = [Conversation]()
    var matches = [User]()

    //MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeView()
        
        conversations = TinderManager.sharedInstance.getConversations()
        tableView.reloadData()

        matches = TinderManager.sharedInstance.getMatches()
        matchesView.collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Conversations"

        if let selectionIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectionIndexPath, animated: animated)
        }
    }
    
    //MARK: - UITableView method delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.conversations.count == 0 {
            return 1
        } else {
            return self.conversations.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.conversations.count == 0 {
            return self.view.bounds.height - self.navigationController!.navigationBar.bounds.height
        } else {
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch conversations.count {
        case 0:
            return tableView.dequeueReusableCell(withIdentifier: "Empty Cell")!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ConversationsTBCell
            cell.clearCellData()
            
            let user = conversations[indexPath.row].user
            cell.profilePic.downloadedFrom(url: URL(string: user.profilePicLink!)!)
            cell.nameLabel.text = user.username
            
            let lastMessage = conversations[indexPath.row].lastMessage
            cell.messageLabel.text = lastMessage?.content
            let messageDate = Date.init(timeIntervalSince1970: TimeInterval((lastMessage?.timestamp)!))
            let dataformatter = DateFormatter.init()
            dataformatter.timeStyle = .short
            let date = dataformatter.string(from: messageDate)
            cell.timeLabel.text = date
            cell.profilePic.layer.borderColor = GlobalVariables.blue.cgColor
            cell.profilePic.layer.borderWidth = 1
            
            if lastMessage?.owner == .sender && lastMessage?.isRead == false {
                cell.nameLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 17.0)
                cell.messageLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 14.0)
                cell.timeLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 13.0)
                cell.messageLabel.textColor = GlobalVariables.blue
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.conversations.count > 0 {
            showConversation(self.conversations[indexPath.row])
        }
    }
    
    //MARK: - Private methods
    private func customizeView() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil

        // Matches view
        matchesView.customizeIn(self)
        
        // Navigation bar customization
        let navigationTitleFont = UIFont(name: "AvenirNext-Regular", size: 18)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: navigationTitleFont, NSAttributedStringKey.foregroundColor: UIColor.white]
        
        // Right bar button
        let icon = UIImage.init(named: "compose")?.withRenderingMode(.alwaysOriginal)
        let rightButton = UIBarButtonItem.init(image: icon!, style: .plain, target: self, action: #selector(ConversationsVC.showMatches))
        self.navigationItem.rightBarButtonItem = rightButton
        
    }

    @IBAction func closeView(_ sender: Any) {
        matchesView.dismissIn(self)
    }
    
    // Shows matches extra view
    @objc func showMatches() {
        matchesView.showIn(self)
    }

    //Shows ChatVC with given user
    func showConversation(_ conversation: Conversation) {
        self.navigationItem.title = ""
        self.performSegue(withIdentifier: "chat", sender: conversation)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "chat") {
            if let secondViewController = segue.destination as? ChatVC,
                let conversation = sender as? Conversation {
                secondViewController.conversation = conversation
            }
        }
    }
}
