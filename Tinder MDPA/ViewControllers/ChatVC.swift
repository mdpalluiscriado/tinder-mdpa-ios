//
//  ChatVC.swift
//  Tinder MDPA
//
//  Created by Lluís Criado on 18/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit
import SDWebImage

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  UINavigationControllerDelegate {
    
    //MARK: Properties
    @IBOutlet var inputBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    private var timeInterval: TimeInterval = 3
    private var chatTimer: Timer?
    
    var messages = [Message]()
    let barHeight: CGFloat = 50
    var currentUser = TinderManager.sharedInstance.getLoggedUser()
    var conversation: Conversation?

    lazy var successClosure: OnSuccessClosure = { (data) in
        if let messagesArray = data as? [[String: Any]] {
            self.messages = [Message]()
            for message in messagesArray {
                let messageObject = Message(with: message)
                self.messages.append(messageObject)
                self.conversation?.messages?.append(messageObject)
            }
            self.refreshMessagesListUI()
        }
    }

    //MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customization()
        startSyncingWithServer()
        
        if(conversation != nil) {
            loadMessages()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.inputBar.backgroundColor = UIColor.clear
        self.view.layoutIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        // Mark messages as read
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: IBActions
    @IBAction func showMessage(_ sender: Any) {
        animateExtraButtons(toHide: true)
    }
    
    @IBAction func showOptions(_ sender: Any) {
        animateExtraButtons(toHide: false)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if let text = inputTextField.text,
            text.count > 0 {
            composeMessage(text)
            inputTextField.text = ""
        }
    }
    
    //MARK: - NotificationCenter handlers
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            tableView.contentInset.bottom = height
            tableView.scrollIndicatorInsets.bottom = height
            if messages.count > 0 {
                tableView.scrollToRow(at: IndexPath.init(row: messages.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: - UITableView delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch messages[indexPath.row].owner {
        case .receiver:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            cell.clearCellData()
            cell.message.text = messages[indexPath.row].content

            return cell
            
        case .sender:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            cell.profilePic.downloadedFrom(url: URL(string: self.currentUser!.profilePicLink!)!)
            cell.message.text = messages[indexPath.row].content
            cell.messageBackground.image = nil

            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        inputTextField.resignFirstResponder()
    }
    
    //MARK: - UITextField delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendMessage(textField)
        return true
    }
    
    func composeMessage(_ content: String)  {
        let message = Message(type: .text,
                              content: content,
                              fromId: (TinderManager.sharedInstance.getLoggedUser()?.userId!)!,
                              toId: (conversation?.user.userId!)!)

        conversation?.messages?.append(message)
        messages.append(message)
        refreshMessagesListUI()

        let requestBody = createRequestBody(user: (conversation?.user)!, message: message)
        let successClosure: OnSuccessClosure = { (data) in print("TODO: handle success") }

        if let conversationId = conversation?.conversationId {
            NetworkManager.sharedInstance.sendTextMessageForExistingConversation(conversationId: conversationId,
                                                                                 requestBody: requestBody,
                                                                                 onSuccessClousure: successClosure)
        } else {
            NetworkManager.sharedInstance.sendTextMessageForNewConversation(requestBody: requestBody,
                                                                            onSuccessClousure: successClosure)
        }
    }

    //MARK: - Private Methods
    //MARK: - UI Methods
    private func customization() {
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.showKeyboard(notification:)),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)

        tabBarController?.tabBar.isHidden = true
        navigationItem.title = conversation?.user.username
        navigationItem.backBarButtonItem?.title = ""
        inputAccessoryView?.isHidden = false

        if #available(iOS 11.0, *)  {   tableView.estimatedRowHeight = UITableViewAutomaticDimension    }
        else                        {   tableView.estimatedRowHeight = 10000    }

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.contentInset.bottom = barHeight
        tableView.scrollIndicatorInsets.bottom = barHeight
    }

    private func animateExtraButtons(toHide: Bool)  {
        switch toHide {
        case true:
            bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        default:
            bottomConstraint.constant = -50
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        }
    }

    // MARK: Timer methods
    private func startSyncingWithServer() {
        stopSyncingWithServer()

        let timer = Timer(timeInterval: timeInterval,
                          target: self,
                          selector: #selector(loadMessages),
                          userInfo: nil,
                          repeats: true)

        RunLoop.main.add(timer, forMode: RunLoopMode.commonModes)
        chatTimer = timer
        chatTimer?.fire()
    }

    private func stopSyncingWithServer() {
        chatTimer?.invalidate()
        chatTimer = nil
    }

    // MARK: - Logic methods
    @objc private func loadMessages() {
        NetworkManager.sharedInstance.fetchMessagesForConversation(conversation!,
                                                                   onSuccessClousure: successClosure)
    }

    private func createRequestBody(user: User, message: Message) -> [String: Any] {
        var requestBody = [String: Any]()
        var usersArray = [[String: Any]]()

        usersArray.append(user.toDictionary())
        usersArray.append((TinderManager.sharedInstance.getLoggedUser()?.toDictionary())!)

        requestBody["users"] = usersArray
        requestBody["message"] = message.toDictionary()

        return requestBody;
    }

    private func refreshMessagesListUI(){
        messages.sort{ $0.timestamp < $1.timestamp }
        if !messages.isEmpty {
            CATransaction.begin()
            tableView.reloadData()

            tableView.scrollToRow(at: IndexPath.init(row: messages.count - 1, section: 0),
                                  at: .bottom,
                                  animated: false)
            CATransaction.commit()
        }
    }

    override var inputAccessoryView: UIView? {
        get {
            self.inputBar.frame.size.height = self.barHeight
            self.inputBar.clipsToBounds = true
            return self.inputBar
        }
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }
}
