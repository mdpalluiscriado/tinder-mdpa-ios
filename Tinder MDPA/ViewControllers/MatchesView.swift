//
//  MatchesView.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 22/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class MatchesView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!

    var conversationVC: ConversationsVC!
    
    var topConstraint: NSLayoutConstraint!
    let darkView = UIView.init()
    
    //--------------------------------------------------------------------------
    public func customizeIn(_ conversationsView: ConversationsVC) {
        
        self.conversationVC = conversationsView
        
        // DarkView customization
        conversationsView.view.addSubview(darkView)
        self.darkView.backgroundColor = UIColor.black
        self.darkView.alpha = 0
        self.darkView.translatesAutoresizingMaskIntoConstraints = false
        self.darkView.leadingAnchor.constraint(equalTo: conversationsView.view.leadingAnchor).isActive = true
        self.darkView.topAnchor.constraint(equalTo: conversationsView.view.topAnchor).isActive = true
        self.darkView.trailingAnchor.constraint(equalTo: conversationsView.view.trailingAnchor).isActive = true
        self.darkView.bottomAnchor.constraint(equalTo: conversationsView.view.bottomAnchor).isActive = true
        self.darkView.isHidden = true
        
        // Container view customization
        let extraViewsContainer = UIView.init()
        extraViewsContainer.translatesAutoresizingMaskIntoConstraints = false
        conversationsView.view.addSubview(extraViewsContainer)
        topConstraint = NSLayoutConstraint.init(item: extraViewsContainer,
                                                                       attribute: .top,
                                                                       relatedBy: .equal,
                                                                       toItem: conversationsView.view,
                                                                       attribute: .top,
                                                                       multiplier: 1,
                                                                       constant: 1000)
        topConstraint.isActive = true
        extraViewsContainer.leadingAnchor.constraint(equalTo: conversationsView.view.leadingAnchor).isActive = true
        extraViewsContainer.trailingAnchor.constraint(equalTo: conversationsView.view.trailingAnchor).isActive = true
        extraViewsContainer.heightAnchor.constraint(equalTo: conversationsView.view.heightAnchor, multiplier: 1).isActive = true
        extraViewsContainer.backgroundColor = UIColor.clear
        
        extraViewsContainer.addSubview(self)
        
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: extraViewsContainer.topAnchor).isActive = true
        leadingAnchor.constraint(equalTo: extraViewsContainer.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: extraViewsContainer.trailingAnchor).isActive = true
        bottomAnchor.constraint(equalTo: extraViewsContainer.bottomAnchor).isActive = true
        isHidden = true
        collectionView?.contentInset = UIEdgeInsetsMake(10, 0, 0, 0)
        backgroundColor = UIColor.clear
    }
    
    //--------------------------------------------------------------------------
    public func showIn(_ conversationsView: ConversationsVC) {
        let transform = CGAffineTransform.init(scaleX: 0.94, y: 0.94)
        topConstraint.constant = 0
        self.darkView.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            conversationsView.view.layoutIfNeeded()
            self.darkView.alpha = 0.8
            conversationsView.view.transform = transform
        })
        
        self.isHidden = false
    }
    
    //--------------------------------------------------------------------------
    public func dismissIn(_ conversationsView: ConversationsVC) {
        topConstraint.constant = 1000
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            conversationsView.view.layoutIfNeeded()
            self.darkView.alpha = 0
            conversationsView.view.transform = CGAffineTransform.identity
        }, completion:  { (true) in
            self.darkView.isHidden = true
            self.isHidden = true
        })
    }
    
    //MARK: - UICollectionView delegate methods
    //--------------------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if conversationVC.matches.count == 0 {
            return 1
        } else {
            return conversationVC.matches.count
        }
    }
    
    //--------------------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if conversationVC.matches.count == 0 {
            return collectionView.dequeueReusableCell(withReuseIdentifier: "Empty Cell", for: indexPath)
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MatchCVCell
            
            let user = conversationVC.matches[indexPath.row]
            cell.profilePic.downloadedFrom(url: URL(string: user.profilePicLink!)!)
            cell.profilePic.layer.borderWidth = 2
            cell.profilePic.layer.borderColor = GlobalVariables.blue.cgColor
            cell.nameLabel.text = user.username
            return cell
        }
    }
    
    //--------------------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if conversationVC.matches.count > 0 {

            self.dismissIn(conversationVC)

            let matchSelected = conversationVC.matches[indexPath.row]

            // check if conversation with selected "match" exists
            if let conversation = isExistingConversation(matchId: matchSelected.userId!) {
                conversationVC.showConversation(conversation)
            } else {
                let conversation = Conversation(conversationId: nil, user: matchSelected, lastMessage: nil)
                conversationVC.showConversation(conversation)
            }
        }
    }
    
    //--------------------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //--------------------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //--------------------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if conversationVC.matches.count == 0 {
            return self.collectionView.bounds.size
        } else {
            if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
                let width = (0.3 * UIScreen.main.bounds.height)
                let height = width + 30
                return CGSize.init(width: width, height: height)
            } else {
                let width = (0.3 * UIScreen.main.bounds.width)
                let height = width + 30
                return CGSize.init(width: width, height: height)
            }
        }
    }

    private func isExistingConversation(matchId: String) -> Conversation? {
        return conversationVC.conversations.filter { $0.user.userId == matchId }.first
    }
}

