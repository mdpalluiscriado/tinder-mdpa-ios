//
//  PresentUserDetailView.swift
//  Tinder MDPA
//
//  Created by Lluís Criado on 18/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class PresentUserDetailView: UIView {

    var topConstraint: NSLayoutConstraint!
    
    public func customizeIn(tinderVC: TinderVC) {
        tinderVC.view.insertSubview(self, belowSubview: tinderVC.cloudsView)
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: tinderVC.view.centerXAnchor).isActive = true
        topConstraint = NSLayoutConstraint.init(item: self, attribute: .top, relatedBy: .equal, toItem: tinderVC.view, attribute: .top, multiplier: 1, constant: 1000)
        topConstraint.isActive = true
        heightAnchor.constraint(equalTo: tinderVC.view.heightAnchor, multiplier: 0.75).isActive = true
        widthAnchor.constraint(equalTo: tinderVC.view.widthAnchor, multiplier: 0.9).isActive = true
        layer.cornerRadius = 8
    }
}
