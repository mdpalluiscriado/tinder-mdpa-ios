//
//  LoginView.swift
//  Tinder MDPA
//
//  Created by Lluís Criado on 18/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    var topConstraint: NSLayoutConstraint!
    
    public func customizeIn(welcomeView: WelcomeVC) {
        welcomeView.view.insertSubview(self, belowSubview: welcomeView.cloudsView)
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: welcomeView.view.centerXAnchor).isActive = true
        topConstraint = NSLayoutConstraint.init(item: self, attribute: .top, relatedBy: .equal, toItem: welcomeView.view, attribute: .top, multiplier: 1, constant: 60)
        topConstraint.isActive = true
        heightAnchor.constraint(equalTo: welcomeView.view.heightAnchor, multiplier: 0.45).isActive = true
        widthAnchor.constraint(equalTo: welcomeView.view.widthAnchor, multiplier: 0.9).isActive = true
        layer.cornerRadius = 8
    }
}
