//
//  EditProfileView.swift
//  Tinder MDPA
//
//  Created by Lluís Criado on 18/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class EditProfileView: UIView {
    
    var topConstraint: NSLayoutConstraint!

    public func customizeIn(settingsVC: SettingsVC) {
        settingsVC.view.insertSubview(self, aboveSubview: settingsVC.cloudsView)
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: settingsVC.view.centerXAnchor).isActive = true
        topConstraint = NSLayoutConstraint.init(item: self, attribute: .top, relatedBy: .equal, toItem: settingsVC.view, attribute: .top, multiplier: 1, constant: 1000)
        topConstraint.isActive = true
        heightAnchor.constraint(equalTo: settingsVC.view.heightAnchor, multiplier: 0.9).isActive = true
        widthAnchor.constraint(equalTo: settingsVC.view.widthAnchor, multiplier: 0.9).isActive = true
        layer.cornerRadius = 8
    }
}
