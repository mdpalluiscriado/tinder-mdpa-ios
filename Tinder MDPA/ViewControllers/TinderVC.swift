//
//  PhotosVC.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 14/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class TinderVC: UIViewController {
    
    // MARK: Properties
    // Present user view
    @IBOutlet var presentUserView: PresentUserView!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    
    // Present user detail view
    @IBOutlet var presentUserDetailView: PresentUserDetailView!
    @IBOutlet var userDetailImageView: UIImageView!
    @IBOutlet var usernameDetailLabel: UILabel!
    @IBOutlet var ageDetailLabel: UILabel!
    
    // Tinder view
    @IBOutlet weak var cloudsView: UIImageView!
    @IBOutlet weak var likeButton: RoundedButton!
    @IBOutlet weak var dislikeButton: RoundedButton!


    var candidates = [User]()
    var userIndex = 0

    // MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        customization()
        addGestureRecognizers()
        prepareCandidates()

        presentCandidate()
    }
    
    // MARK: IBActions
    @IBAction func likeUser(_ sender: Any) {
        candidateSeen(isLiked: true)
    }
    
    @IBAction func dislikeUser(_ sender: Any) {
        candidateSeen(isLiked: false)
    }

    // MARK: Private Methods
    // MARK: - UI Methods
    private func customization() {
        presentUserView.customizeIn(tinderVC: self)
        presentUserDetailView.customizeIn(tinderVC: self)
    }

    private func addGestureRecognizers() {
        addGestureRecognizer(userImageView)
        addGestureRecognizer(userDetailImageView)
    }

    private func addGestureRecognizer(_ imageView: UIImageView) {
        imageView.isUserInteractionEnabled = true

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.addGestureRecognizer(tapGestureRecognizer)

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        imageView.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        imageView.addGestureRecognizer(swipeLeft)
    }

    @objc private func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        switchViewsAnimation()
    }

    @objc private func swiped(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                candidateSeen(isLiked: true)
            case UISwipeGestureRecognizerDirection.left:
                candidateSeen(isLiked: false)
            default:
                break
            }
        }
    }

    private func switchViewsAnimation() {
        if(presentUserView.topConstraint.constant == 40) {
            presentUserView.topConstraint.constant = 1000
            presentUserDetailView.topConstraint.constant = 40
        } else {
            presentUserView.topConstraint.constant = 40
            presentUserDetailView.topConstraint.constant = 1000
        }

        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: { self.view.layoutIfNeeded() }) { (finished) in }
    }

    // MARK: - Logic methods
    private func prepareCandidates() {
        let allCandidates = TinderManager.sharedInstance.getCandidates()

        var filteredCandidates = [User]()
        for candidate in allCandidates {
            if(isLoggedUser(candidate) ||           // Checks if candidate is logged user
                isInUsersSeen(candidate) ||         // Checks if logged user has already seen the candidate
                isOutAgeRange(candidate) ||         // Checks if candidate age is inside logged user preferences
                isOutDistanceRange(candidate) ||    // Checks if candidate location is inside logged user preferences
                isGenderMatching(candidate))        // Checks if candidate gender matches with logged user preferences
            {
                print("[TINDER_DEBUG]: candidate discarted: " + candidate.userId!)
            } else {
                filteredCandidates.append(candidate)
            }
        }
        candidates = filteredCandidates
    }

    private func candidateSeen(isLiked: Bool) {
        if (candidates.count == 0 || userIndex >= candidates.count) {
            noMoreCandidates()
            return
        }

        let currentCandidate = candidates[userIndex]

        let requestBody = createUserSeenRequestBody(candidateId: (currentCandidate.userId)!, isLiked: isLiked)

        let successClosure: OnSuccessClosure = { (data) in

            if let dataJson = data as? [String: Any],
                let isMatch = dataJson["match"] as? Bool {
                print("[TINDER_DEBUG]: isMatch -> " + String(isMatch))
            }

            self.userIndex += 1
            self.presentCandidate()
        }

        NetworkManager.sharedInstance.addUserSeen(requestBody: requestBody,
                                                  onSuccessClousure: successClosure)
    }

    private func presentCandidate() {
        if (candidates.count == 0 || userIndex >= candidates.count) {
            noMoreCandidates()
            return
        }

        switchViewsAnimation()

        let candidate = candidates[userIndex]
        // Set basic information
        usernameLabel.text = candidate.username
        userImageView.downloadedFrom(url: URL(string: candidate.profilePicLink!)!)

        // Set detail information
        usernameDetailLabel.text = candidate.username
        userDetailImageView.downloadedFrom(url: URL(string: candidate.profilePicLink!)!)
    }

    private func noMoreCandidates() {
        dislikeButton.isHidden = true
        likeButton.isHidden = true

        userImageView.image = #imageLiteral(resourceName: "sad")
        usernameLabel.text = "Unfortunately there are no more users!"

        userDetailImageView.image = #imageLiteral(resourceName: "sad")
        usernameDetailLabel.text = "Unfortunately there are no more users!"
    }

    private func isLoggedUser(_ candidate: User) -> Bool {
        return candidate.userId == TinderManager.sharedInstance.getLoggedUser()?.userId
    }

    private func isInUsersSeen(_ candidate: User) -> Bool {
        if let usersSeen = TinderManager.sharedInstance.getLoggedUser()?.usersSeen {
            return usersSeen.filter { $0.userSeenId == candidate.userId }.first != nil
        }
        return false
    }

    private func isOutAgeRange(_ candidate: User) -> Bool {
        return false // Misses implementation
    }

    private func isOutDistanceRange(_ candidate: User) -> Bool {
        return false // Misses implementation
    }

    private func isGenderMatching(_ candidate: User) -> Bool {
        return false // Misses implementation
    }

    private func createUserSeenRequestBody(candidateId: String, isLiked: Bool) -> [String: Any] {
        var requestBody = [String: Any]()
        requestBody["userSeenId"] = candidateId
        requestBody["isLiked"] = isLiked
        return requestBody
    }
}
