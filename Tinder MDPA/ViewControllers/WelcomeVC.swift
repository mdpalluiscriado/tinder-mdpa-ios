//
//  LoginVC.swift
//  Tinder MDPA
//
//  Created by Lluis Criado Rueda on 15/11/2017.
//  Copyright © 2017 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController, UITextFieldDelegate {
    
    // MARK: Properties
    // login view
    @IBOutlet var loginView: LoginView!
    @IBOutlet weak var loginEmailField: UITextField!
    @IBOutlet weak var loginPasswordField: UITextField!
    @IBOutlet var signInButton: RoundedButton!
    
    // register view
    @IBOutlet var registerView: RegisterView!
    @IBOutlet weak var registerNameField: UITextField!
    @IBOutlet weak var registerPasswordField: UITextField!
    @IBOutlet var registerButton: RoundedButton!
    @IBOutlet weak var switchViewsButton: UIButton!
    @IBOutlet var waringLabels: [UILabel]!
    @IBOutlet var termsSwitch: UISwitch!
    
    // welcome view
    @IBOutlet weak var cloudsView: UIImageView!
    @IBOutlet weak var cloudsViewLeading: NSLayoutConstraint!
    
    var isLoginViewVisible = true

    lazy var successClosure: OnSuccessClosure = { (data) in
        TinderManager.sharedInstance.setLoggedUser(User(fromLogin: data as! [String:Any]))

        NetworkManager.sharedInstance.fetchCandidates()
        NetworkManager.sharedInstance.fetchMatchesForCurrentUser()
        NetworkManager.sharedInstance.fetchConversationsForCurrentUser()

        self.performSegue(withIdentifier: "main", sender: nil)
    }
    
    //MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        customization()
    }
    
    //MARK: - UITextField delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        resetWarningLabels(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if isLoginViewVisible {
            login(textField)
        } else {
            register(textField)
        }
        
        return true
    }
    
    //MARK: IBActions
    @IBAction func switchViews(_ sender: UIButton) {
        switchConstants(sender)
        resetWarningLabels(true)
    }
    
    @IBAction func register(_ sender: Any) {
        registerButton.isEnabled = false
        
        let usernameText = registerNameField.text
        let passwordText = registerPasswordField.text
        
        if (usernameText == "" || passwordText == "" || !termsSwitch.isOn) {
            resetRegister()
            return
        }

        let requestBody = createRequestBody(email: usernameText, password: passwordText)
        let failureClosure: OnFailureClosure = { (error) in self.resetRegister() }

        NetworkManager.sharedInstance.signupUser(requestBody: requestBody,
                                                 onSuccessClousure: successClosure,
                                                 onFailureClousure: failureClosure)
    }
    
    @IBAction func login(_ sender: Any) {
        signInButton.isEnabled = false
        
        let emailText = loginEmailField.text
        let passwordText = loginPasswordField.text

        if (emailText == "" || passwordText == "") {
            resetLogin()
            return
        }

        let requestBody = createRequestBody(email: emailText, password: passwordText)
        let failureClosure: OnFailureClosure = { (error) in self.resetLogin() }

        NetworkManager.sharedInstance.loginUser(requestBody: requestBody,
                                                onSuccessClousure: successClosure,
                                                onFailureClousure: failureClosure)
    }
    
    //MARK: Private methods
    private func customization() {
        loginView.customizeIn(welcomeView: self)
        registerView.customizeIn(welcomeView: self)
        cloudsViewLeading.constant = 0
    }

    private func switchConstants(_ sender: UIButton) {
        switch isLoginViewVisible {
        case true:
            isLoginViewVisible = false
            sender.setTitle("Login", for: .normal)
            loginView.topConstraint.constant = 1000
            registerView.topConstraint.constant = 60

        case false:
            isLoginViewVisible = true
            sender.setTitle("Create New Account", for: .normal)
            loginView.topConstraint.constant = 60
            registerView.topConstraint.constant = 1000
        }

        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: { self.view.layoutIfNeeded()}) { (finished) in }
    }

    private func resetRegister() {
        resetWarningLabels(false)
        registerButton.isEnabled = true
    }

    private func resetLogin() {
        resetWarningLabels(false)
        signInButton.isEnabled = true
    }

    private func resetWarningLabels(_ value: Bool) {
        for item in self.waringLabels {
            item.isHidden = value
        }
    }
    
    private func createRequestBody(email: String!, password: String!) -> Dictionary<String, String> {
        return ["username": email, "password": password] as [String : String]
    }
}
