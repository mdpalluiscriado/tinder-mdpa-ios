//
//  ConnectionConstants.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 21/08/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

typealias OnSuccessClosure = (Any) -> Void
typealias OnFailureClosure = (Error) -> Void

enum ConnectionType: String {
    case GET
    case POST
    case DELETE
    case UPDATE
}
