//
//  APIConnection.swift
//  Tinder MDPA
//
//  Created by Lluís Criado on 26/04/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import Foundation

public class ApiConnection: NSObject, URLSessionDelegate, URLSessionTaskDelegate {

    private static let sharedInstance = ApiConnection()
    
    private var session = URLSession(configuration: URLSessionConfiguration.ephemeral)
    // private var baseUrl = "http://localhost:3000/"    // Local
    private var baseUrl = "https://mdpalluiscriado.azurewebsites.net/"    // server

    
    private override init() {}
    
    // MARK: - Public methods
    static func get(URLString: String,
                    parameters: Dictionary<String, Any>,
                    onSuccessClousure success: @escaping OnSuccessClosure,
                    onFailureClousure failure: @escaping OnFailureClosure) {
        ApiConnection.sharedInstance.HTTPRequestOperation(method: "GET",
                                                          URLString: URLString,
                                                          parameters: parameters,
                                                          onSuccess: success,
                                                          onFailure: failure)
    }
    
    static func post(URLString: String,
                     parameters: Dictionary<String, Any>,
                     onSuccessClousure success: @escaping OnSuccessClosure,
                     onFailureClousure failure: @escaping OnFailureClosure) {
        ApiConnection.sharedInstance.HTTPRequestOperation(method: "POST",
                                                          URLString: URLString,
                                                          parameters: parameters,
                                                          onSuccess: success,
                                                          onFailure: failure)
    }
    
    // MARK: - Private methods
    private func HTTPRequestOperation(method: String,
                                      URLString: String,
                                      parameters: Any,
                                      onSuccess: @escaping OnSuccessClosure,
                                      onFailure: @escaping OnFailureClosure) {
        if var request = createRequest(method: method, URLString: URLString) {
            
            do {
                if (method == "POST") {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                }
            } catch {}
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            executeHTTPRequest(request: request, onSuccessClosure: onSuccess, onFailureClosure: onFailure)
        }
    }
    
    private func createRequest(method: String, URLString: String) -> URLRequest? {
        if let trackerUrlString = (baseUrl + URLString).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed),
            let trackerUrl = URL(string: trackerUrlString)
        {
            var request = URLRequest(url: trackerUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
            request.httpMethod = method
            return request
        }
        return nil
    }
    
    private func executeHTTPRequest(request: URLRequest,
                                    onSuccessClosure: @escaping OnSuccessClosure,
                                    onFailureClosure: @escaping OnFailureClosure) {
        session.dataTask(with: request, completionHandler:{ data, response, error in
            DispatchQueue.main.async {
                if error == nil,
                    let data = data,
                    let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        do {
                            let dataDictionary = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions())
                            onSuccessClosure(dataDictionary)
                        } catch {
                            onFailureClosure(error)
                        }
                    } else {
                        let error = NSError(domain: "[LOGGER]: ERROR -> " +
                            HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode) +
                            ". Http/https server error", code: httpResponse.statusCode, userInfo: nil )
                        onFailureClosure(error)
                    }
                } else {
                    onFailureClosure(error!)
                }
            }
        }).resume()
    }
}
