
//
//  AppDelegate.swift
//  Tinder MDPA
//
//  Created by Lluis Criado Rueda on 15/11/2017.
//  Copyright © 2017 Lluis Criado Rueda. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    //--------------------------------------------------------------------------
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "navigation")!.resizableImage(withCapInsets: UIEdgeInsetsMake(0, 0, 0, 0), resizingMode: .stretch), for: .default)
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        return true
    }
    
    //--------------------------------------------------------------------------
    func applicationDidEnterBackground(_ application: UIApplication) { }
    
    //--------------------------------------------------------------------------
    func applicationWillEnterForeground(_ application: UIApplication) { }
    
    //--------------------------------------------------------------------------
    func applicationDidBecomeActive(_ application: UIApplication) { }
    
    //--------------------------------------------------------------------------
    func applicationWillTerminate(_ application: UIApplication) { }
}
