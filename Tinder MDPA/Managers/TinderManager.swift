//
//  TinderManager.swift
//  Tinder MDPA
//
//  Created by Lluís Criado on 17/02/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

final class TinderManager {
    
    private var loggedUser: User?
    private var matches = [User]()
    private var candidates = [User]()
    private var conversations = [Conversation]()
    
    static let sharedInstance = TinderManager()
    
    private init() {}
    
    func setLoggedUser(_ loggedUser: User) {
        self.loggedUser = loggedUser
    }
    
    func getLoggedUser() -> User? {
        return loggedUser
    }

    func setCandidates(candidates: [User]) {
        self.candidates = candidates
    }

    func getCandidates() -> [User] {
        return candidates
    }
    
    func setMatches(matches: [User]) {
        self.matches = matches
    }
    
    func getMatches() -> [User] {
        return matches
    }
    
    func setConversations(conversations: [Conversation]) {
        self.conversations = conversations
    }
    
    func getConversations() -> [Conversation] {
        return conversations
    }
}
