//
//  NetworkManager.swift
//  Tinder MDPA
//
//  Created by Lluís Criado on 26/04/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

class NetworkManager {

    static let sharedInstance = NetworkManager()

    let onFailureClousure: OnFailureClosure = { (error) in
        print("[TINDER_ERROR]: " + error.localizedDescription)
    }

    private init() {}

    // MARK - GET requests
    func fetchCandidates() {
        ApiConnection.get(URLString: "users",
                          parameters: [:],
                          onSuccessClousure: { (data) -> Void in

                            if let candidatesArray = data as? [[String: Any]] {
                                var candidates = [User]()
                                for candidate in candidatesArray {
                                    candidates.append(User(fromCandidate: candidate))
                                }
                                print("[TINDER_DEBUG]: We have fetched " + String(candidates.count) + " candidates")
                                TinderManager.sharedInstance.setCandidates(candidates: candidates)
                            }

        }, onFailureClousure: onFailureClousure)
    }

    func fetchMatchesForCurrentUser() {
        ApiConnection.get(URLString: "users/" + (TinderManager.sharedInstance.getLoggedUser()?.userId!)! + "/matches",
                          parameters: [:],
                          onSuccessClousure: { (data) -> Void in

                            if let matchesArray = data as? [[String: Any]] {
                                var matches = [User]()
                                for match in matchesArray {
                                    matches.append(User(fromCandidate: match))
                                }
                                TinderManager.sharedInstance.setMatches(matches: matches)
                            }

        }, onFailureClousure: onFailureClousure)
    }

    func fetchConversationsForCurrentUser() {
        ApiConnection.get(URLString: "users/" + (TinderManager.sharedInstance.getLoggedUser()?.userId!)! + "/conversations",
                          parameters: [:],
                          onSuccessClousure: { (data) -> Void in

                            if let conversationsArray = data as? [[String: Any]] {
                                var conversations = Array<Conversation>()
                                for conversation in conversationsArray {
                                    conversations.append(Conversation(with: conversation))
                                }
                                TinderManager.sharedInstance.setConversations(conversations: conversations)
                            }

        }, onFailureClousure: onFailureClousure)
    }

    func fetchMessagesForConversation(_ conversation: Conversation,
                                      onSuccessClousure: @escaping OnSuccessClosure) {

        if conversation.conversationId != nil {
            ApiConnection.get(URLString: "conversations/" + conversation.conversationId! + "/messages",
                              parameters: [:],
                              onSuccessClousure: onSuccessClousure,
                              onFailureClousure: onFailureClousure)
        }
    }

    // MARK - POST requests
    func loginUser(requestBody: [String: Any],
                   onSuccessClousure: @escaping OnSuccessClosure,
                   onFailureClousure: @escaping OnFailureClosure) {
        ApiConnection.post(URLString: "login",
                           parameters: requestBody,
                           onSuccessClousure: onSuccessClousure,
                           onFailureClousure: onFailureClousure)
    }

    func signupUser(requestBody: [String: Any],
                    onSuccessClousure: @escaping OnSuccessClosure,
                    onFailureClousure: @escaping OnFailureClosure) {
        ApiConnection.post(URLString: "signup",
                           parameters: requestBody,
                           onSuccessClousure: onSuccessClousure,
                           onFailureClousure: onFailureClousure)
    }

    func updateUserValues(requestBody: [String: Any],
                          onSuccessClousure: @escaping OnSuccessClosure) {
        ApiConnection.post(URLString: "users/" + (TinderManager.sharedInstance.getLoggedUser()?.userId)!,
                           parameters: requestBody,
                           onSuccessClousure: onSuccessClousure,
                           onFailureClousure: onFailureClousure)
    }

    func sendTextMessageForNewConversation(requestBody: [String: Any],
                         onSuccessClousure: @escaping OnSuccessClosure) {

        ApiConnection.post(URLString: "conversations",
                           parameters: requestBody,
                           onSuccessClousure: onSuccessClousure,
                           onFailureClousure: onFailureClousure)
    }

    func sendTextMessageForExistingConversation(conversationId: String,
                                                requestBody: [String: Any],
                                                onSuccessClousure: @escaping OnSuccessClosure) {
        ApiConnection.post(URLString: "conversations/" + conversationId + "/messages",
                           parameters: requestBody,
                           onSuccessClousure: onSuccessClousure,
                           onFailureClousure: onFailureClousure)
    }

    func addUserSeen(requestBody: [String: Any],
                     onSuccessClousure: @escaping OnSuccessClosure) {
        ApiConnection.post(URLString: "users/" + (TinderManager.sharedInstance.getLoggedUser()?.userId)! + "/usersSeen",
                           parameters: requestBody,
                           onSuccessClousure: onSuccessClousure,
                           onFailureClousure: onFailureClousure)
    }
}
