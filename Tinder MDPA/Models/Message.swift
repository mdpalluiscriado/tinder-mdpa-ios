//
//  Message.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 08/01/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import UIKit

class Message {
    
    //MARK: Properties
    var owner: MessageOwner
    var type: MessageType
    var content: String
    var timestamp: TimeInterval
    var isRead: Bool
    var image: UIImage?
    private var toId: String?
    private var fromId: String?
    
    //MARK: Inits
    init(type: MessageType, content: String, fromId: String, toId: String) {
        self.type = type
        self.content = content
        self.timestamp = Date().timeIntervalSince1970
        self.isRead = false
        self.fromId = fromId
        self.toId = toId
        owner = .receiver

    }

    init(with dictionary : [String:Any]) {
        self.type = .text
        self.content = dictionary["content"] as! String
        // TODO: implement
        self.timestamp = Date().timeIntervalSince1970
        self.isRead = dictionary["isRead"] as! Bool
        self.toId = dictionary["toId"] as? String
        self.fromId = dictionary["fromId"] as? String

        if fromId == TinderManager.sharedInstance.getLoggedUser()?.userId {
            owner = .receiver
        } else {
            owner = .sender
        }
    }

    //MARK: Public methods
    func toDictionary() -> [String: Any] {
        var messageDict = [String: Any]()
        
        messageDict["content"] = content
        messageDict["fromId"] = fromId
        messageDict["isRead"] = isRead
        messageDict["timestamp"] = timestamp
        messageDict["toId"] = toId
        messageDict["type"] = type.rawValue
        
        return messageDict
    }
}
