//
//  UserSeen.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 21/08/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

class UserSeen {

    var userSeenId: String?
    var isLiked: Bool?

    //MARK: Inits
    init(with dictionary: [String:Any]) {
        self.userSeenId = dictionary["userSeenId"] as? String
        self.isLiked = dictionary["isLiked"] as? Bool
    }
}
