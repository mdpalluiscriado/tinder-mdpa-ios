//
//  Conversation.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 08/01/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

class Conversation {
    
    //MARK: Properties
    var conversationId: String?
    var user: User
    var lastMessage: Message?
    var messages: [Message]?
    
    //MARK: - Inits
    //--------------------------------------------------------------------------
    init(conversationId: String?, user: User, lastMessage: Message?) {
        self.conversationId = conversationId
        self.user = user
        self.lastMessage = lastMessage
    }
    
    init(with dictionary: [String:Any]) {
        conversationId = dictionary["conversationId"] as? String
        let usersDictionary = dictionary["users"] as! [[String: Any]]
        var fetchedUser = User()
        for userDict in usersDictionary {
            // TODO: refactor
            let tmpUser = User(fromConversation: userDict)
            let loggedUser = TinderManager.sharedInstance.getLoggedUser()
            if tmpUser.userId != loggedUser?.userId {
                fetchedUser = tmpUser
            }
        }
        self.user = fetchedUser
        
        if let messagesAr = dictionary["messages"] as? [[String: Any]] {
            messages = Utils.jsonArrayToList(type: "Message", jsonArray: messagesAr) as? [Message]
            let totalMessages = messages?.count
            lastMessage = messages![(totalMessages! - 1)]
        }
    }
    
    //MARK: Public Methods
    func toDictionary() -> [String:Any] {
        var result = [String:Any]()
        result["user"] = self.user.toDictionary()
        return result
    }
}
