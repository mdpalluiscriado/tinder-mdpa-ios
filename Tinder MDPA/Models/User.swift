//
//  User.swift
//  Tinder MDPA
//
//  Created by Criado, Lluis on 08/01/2018.
//  Copyright © 2018 Lluis Criado Rueda. All rights reserved.
//

import Foundation
import UIKit


class User {
    
    // MARK: Properties
    var userId: String?
    var username: String?
    var profilePicLink: String?
    var password: String?
    var email: String?
    var descript: String?
    var currentJob: String?
    var university: String?
    var isMale: Bool?
    var conversations: [String]?
    var matches: [String]?
    var images: [String]?
    var fromAge: Int?
    var toAge: Int?
    var usersSeen: [UserSeen]?

        
    //MARK: Inits
    init() { }

    init(fromLogin dictionary: [String:Any]) {
        if let userDict = dictionary["user"] as? [String:Any] {
            parseUser(userDict)
        }
    }

    init(fromCandidate dictionary: [String:Any]) {
        parseUser(dictionary)
    }

    init(fromConversation dictionary: [String:Any]) {
        self.username        = dictionary["username"] as? String
        self.userId          = dictionary["userId"] as? String
        self.profilePicLink  = dictionary["profilePicLink"] as? String
    }
        
    //MARK: - Public Methods
    func toDictionary() -> [String:Any] {
        var result = [String:Any]()
        result["username"] = self.username
        result["profilePicLink"] = self.profilePicLink
        result["userId"] = self.userId
        return result
    }

    //MARK: - Private Methods
    private func parseUser(_ dictionary: [String:Any]) {
        self.username        = dictionary["username"] as? String
        self.userId          = dictionary["userId"] as? String
        self.profilePicLink  = dictionary["profilePicLink"] as? String
        self.password        = dictionary["password"] as? String
        self.email           = dictionary["email"] as? String
        self.descript        = dictionary["description"] as? String
        self.currentJob      = dictionary["currentJob"] as? String
        self.university      = dictionary["university"] as? String
        self.isMale          = dictionary["isMale"] as? Bool
        self.conversations   = dictionary["conversations"] as? [String]
        self.matches         = dictionary["matches"] as? [String]
        self.images          = dictionary["images"] as? [String]
        if let usersSeenArray = dictionary["usersSeen"] as? [[String: Any]],
            let usersSeen = Utils.jsonArrayToList(type: "UserSeen", jsonArray: usersSeenArray) as? [UserSeen] {
            self.usersSeen = usersSeen
        }
        if let ageRange = dictionary["ageRangePreference"] as? [String: Any] {
            self.fromAge = ageRange["from"] as? Int
            self.toAge = ageRange["to"] as? Int
        }
    }
}
